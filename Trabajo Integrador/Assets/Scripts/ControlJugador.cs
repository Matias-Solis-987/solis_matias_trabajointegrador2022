﻿using UnityEngine;
public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public Camera camaraPrimeraPersona;
    public LayerMask capaPiso;
    public LayerMask capaBot;
    public float magnitudSalto;
    public CapsuleCollider col;
    public GameObject proyectil;
    private Rigidbody rb;
    public float CantidadSalto = 2;
    public float Salto = 1;
    public float RecargaSalto = 2;
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();
    }
    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);
            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * 15, ForceMode.Impulse);
            Destroy(pro, 5);
            RaycastHit hit;
            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 5)
            {
                Debug.Log("El rayo tocó al objeto: " + hit.collider.name);
            }
            if (hit.collider.name.Substring(0, 3) == "Bot")
            {
                GameObject objetoTocado = GameObject.Find(hit.transform.name);
                ControlBot scriptObjetoTocado = (ControlBot)objetoTocado.GetComponent(typeof(ControlBot));
                if (scriptObjetoTocado != null)
                {
                    scriptObjetoTocado.recibirDaño();
                }
            }
        }
    }
    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space) && CantidadSalto > 0 && EstaEnPiso() || EstaEnBot())
        {
            CantidadSalto = CantidadSalto - Salto;
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
        }
        bool EstaEnPiso()
        {
            CantidadSalto = RecargaSalto;
            return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
        }
        bool EstaEnBot()
        {
            CantidadSalto = RecargaSalto;
            return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaBot);
        }
    }
}
